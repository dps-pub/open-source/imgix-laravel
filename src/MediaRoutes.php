<?php
/**
 * This file is generated with `make generate`
 * DO NOT EDIT | generated at Wed, 28 Mar 2018 15:59:53 +1030
 */
namespace DPS\Imgix\Laravel;

use DPS\Imgix\UrlBuilder;

/**
 * MediaRoutes wrapper around the imgix api
 *
 * @version 8.1.0
 * @link    https://docs.imgix.com/apis/url
 */
trait MediaRoutes
{
    /**
     * __toString() so you can just cast the class to get the url
     *
     * @return string
     */
    public function __toString()
    {
        return (string) app('imgix')->file($this->location);
    }

    /**
     * Applies automatic enhancements to images.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/auto
     *
     * @return $this
     */
    public function autoFeatures($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->autoFeatures($v0);
    }

    /**
     * Colors the background of padded images.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/bg
     *
     * @return $this
     */
    public function backgroundColor($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->backgroundColor($v0);
    }

    /**
     * Specifies the location of the blend image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/blending/blend
     *
     * @return $this
     */
    public function blend($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->blend($v0);
    }

    /**
     * Changes the blend alignment relative to the parent image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/blending/ba
     *
     * @return $this
     */
    public function blendAlign($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->blendAlign($v0);
    }

    /**
     * Changes the alpha of the blend image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/blending/balph
     *
     * @return $this
     */
    public function blendAlpha($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->blendAlpha($v0);
    }

    /**
     * Specifies the type of crop for blend images.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/blending/bc
     *
     * @return $this
     */
    public function blendCrop($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->blendCrop($v0);
    }

    /**
     * Specifies the fit mode for blend images.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/blending/bf
     *
     * @return $this
     */
    public function blendFit($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->blendFit($v0);
    }

    /**
     * Adjusts the height of the blend image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/blending/bh
     *
     * @return $this
     */
    public function blendHeight($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->blendHeight($v0);
    }

    /**
     * Sets the blend mode for a blend image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/blending/bm
     *
     * @return $this
     */
    public function blendMode($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->blendMode($v0);
    }

    /**
     * Applies padding to the blend image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/blending/bp
     *
     * @return $this
     */
    public function blendPadding($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->blendPadding($v0);
    }

    /**
     * Adjusts the size of the blend image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/blending/bs
     *
     * @return $this
     */
    public function blendSize($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->blendSize($v0);
    }

    /**
     * Adjusts the width of the blend image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/blending/bw
     *
     * @return $this
     */
    public function blendWidth($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->blendWidth($v0);
    }

    /**
     * Adjusts the x-offset of the blend image relative to its parent.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/blending/bx
     *
     * @return $this
     */
    public function blendX($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->blendX($v0);
    }

    /**
     * Adjusts the y-offset of the blend image relative to its parent.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/blending/by
     *
     * @return $this
     */
    public function blendY($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->blendY($v0);
    }

    /**
     * Applies a gaussian blur to an image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/stylize/blur
     *
     * @return $this
     */
    public function blur($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->blur($v0);
    }

    /**
     * Applies a border to an image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/border-and-padding/border
     *
     * @return $this
     */
    public function border($v0 = null, $v1 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->border($v0, $v1);
    }

    /**
     * Sets the outer radius of the image's border in pixels.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/border-and-padding/border-radius
     *
     * @return $this
     */
    public function borderRadius($v0 = null, $v1 = null, $v2 = null, $v3 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->borderRadius($v0, $v1, $v2, $v3);
    }

    /**
     * Sets the inner radius of the image's border in pixels.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/border-and-padding/border-radius-inner
     *
     * @return $this
     */
    public function borderRadiusInner($v0 = null, $v1 = null, $v2 = null, $v3 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->borderRadiusInner($v0, $v1, $v2, $v3);
    }

    /**
     * Adjusts the brightness of the source image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/adjustment/bri
     *
     * @return $this
     */
    public function brightness($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->brightness($v0);
    }

    /**
     * Specifies the output chroma subsampling rate.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/format/chromasub
     *
     * @return $this
     */
    public function chromaSubsampling($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->chromaSubsampling($v0);
    }

    /**
     * Sets one or more Client-Hints headers
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/format/ch
     *
     * @return $this
     */
    public function clientHints($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->clientHints($v0);
    }

    /**
     * Specifies how many colors to include in a palette-extraction response.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/color-palette/colors
     *
     * @return $this
     */
    public function colorCount($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->colorCount($v0);
    }

    /**
     * Limits the number of unique colors in an image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/format/colorquant
     *
     * @return $this
     */
    public function colorQuantization($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->colorQuantization($v0);
    }

    /**
     * Specifies the color space of the output image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/format/cs
     *
     * @return $this
     */
    public function colorspace($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->colorspace($v0);
    }

    /**
     * Adjusts the contrast of the source image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/adjustment/con
     *
     * @return $this
     */
    public function contrast($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->contrast($v0);
    }

    /**
     * Specifies the radius value for a rounded corner mask.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/mask/corner-radius
     *
     * @return $this
     */
    public function cornerRadius($v0 = null, $v1 = null, $v2 = null, $v3 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->cornerRadius($v0, $v1, $v2, $v3);
    }

    /**
     * Specifies how to crop an image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/size/crop
     *
     * @return $this
     */
    public function cropMode($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->cropMode($v0);
    }

    /**
     * Crops an image to a specified rectangle.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/size/rect
     *
     * @return $this
     */
    public function cropRectangle($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->cropRectangle($v0);
    }

    /**
     * Specifies a CSS prefix for all classes in palette-extraction.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/color-palette/prefix
     *
     * @return $this
     */
    public function cssPrefix($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->cssPrefix($v0);
    }

    /**
     * Adjusts the device-pixel ratio of the output image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/dpr
     *
     * @return $this
     */
    public function devicePixelRatio($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->devicePixelRatio($v0);
    }

    /**
     * Sets the DPI value in the EXIF header.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/format/dpi
     *
     * @return $this
     */
    public function dotsPerInch($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->dotsPerInch($v0);
    }

    /**
     * Forces a URL to use send-file in its response.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/format/dl
     *
     * @return $this
     */
    public function download($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location."?dl=".$this->name)->download($v0);
    }

    /**
     * Adjusts the exposure of the output image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/adjustment/exp
     *
     * @return $this
     */
    public function exposure($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->exposure($v0);
    }

    /**
     * Selects a face to crop to.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/face-detection/faceindex
     *
     * @return $this
     */
    public function faceIndex($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->faceIndex($v0);
    }

    /**
     * Adjusts padding around a selected face.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/face-detection/facepad
     *
     * @return $this
     */
    public function facePadding($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->facePadding($v0);
    }

    /**
     * Specifies that face data should be included in output when combined with `fm=json`.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/face-detection/faces
     *
     * @return $this
     */
    public function faces($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->faces($v0);
    }

    /**
     * Specifies how to map the source image to the output image dimensions.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/size/fit
     *
     * @return $this
     */
    public function fitMode($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->fitMode($v0);
    }

    /**
     * Flips an image on a specified axis.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/rotation/flip
     *
     * @return $this
     */
    public function flipDirection($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->flipDirection($v0);
    }

    /**
     * Displays crosshairs identifying the location of the set focal point
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/focalpoint-crop/fp-debug
     *
     * @return $this
     */
    public function focalPointCrosshairs($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->focalPointCrosshairs($v0);
    }

    /**
     * Sets the relative horizontal value for the focal point of an image
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/focalpoint-crop/fp-x
     *
     * @return $this
     */
    public function focalPointXValue($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->focalPointXValue($v0);
    }

    /**
     * Sets the relative vertical value for the focal point of an image
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/focalpoint-crop/fp-y
     *
     * @return $this
     */
    public function focalPointYValue($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->focalPointYValue($v0);
    }

    /**
     * Sets the relative zoom value for the focal point of an image
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/focalpoint-crop/fp-z
     *
     * @return $this
     */
    public function focalPointZoomValue($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->focalPointZoomValue($v0);
    }

    /**
     * Adjusts the gamma of the source image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/adjustment/gam
     *
     * @return $this
     */
    public function gamma($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->gamma($v0);
    }

    /**
     * Applies a half-tone effect to the source image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/stylize/htn
     *
     * @return $this
     */
    public function halftone($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->halftone($v0);
    }

    /**
     * Adjusts the height of the output image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/size/h
     *
     * @return $this
     */
    public function height($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->height($v0);
    }

    /**
     * Adjusts the highlights of the source image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/adjustment/high
     *
     * @return $this
     */
    public function high($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->high($v0);
    }

    /**
     * Adjusts the hue of the source image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/adjustment/hue
     *
     * @return $this
     */
    public function hueShift($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->hueShift($v0);
    }

    /**
     * Inverts the colors on the source image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/adjustment/invert
     *
     * @return $this
     */
    public function invert($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->invert($v0);
    }

    /**
     * Specifies that the output image should be a lossless variant.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/format/lossless
     *
     * @return $this
     */
    public function lossless($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->lossless($v0);
    }

    /**
     * Defines the type of mask and specifies the URL if that type is selected.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/mask
     *
     * @return $this
     */
    public function mask($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->mask($v0);
    }

    /**
     * Colors the background of the transparent mask area of images
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/mask/mask-bg
     *
     * @return $this
     */
    public function maskBackgroundColor($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->maskBackgroundColor($v0);
    }

    /**
     * Specifies the maximum height of the output image in pixels.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/size/max-h
     *
     * @return $this
     */
    public function maximumHeight($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->maximumHeight($v0);
    }

    /**
     * Specifies the maximum width of the output image in pixels.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/size/max-w
     *
     * @return $this
     */
    public function maximumWidth($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->maximumWidth($v0);
    }

    /**
     * Specifies the minimum height of the output image in pixels.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/size/min-h
     *
     * @return $this
     */
    public function minimumHeight($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->minimumHeight($v0);
    }

    /**
     * Specifies the minimum width of the output image in pixels.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/size/min-w
     *
     * @return $this
     */
    public function minimumWidth($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->minimumWidth($v0);
    }

    /**
     * Applies a monochrome effect to the source image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/stylize/mono
     *
     * @return $this
     */
    public function monochrome($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->monochrome($v0);
    }

    /**
     * Reduces the noise in an image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/noise-reduction/nr
     *
     * @return $this
     */
    public function noiseBlur($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->noiseBlur($v0);
    }

    /**
     * Provides a threshold by which to sharpen an image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/noise-reduction/nrs
     *
     * @return $this
     */
    public function noiseSharpen($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->noiseSharpen($v0);
    }

    /**
     * Changes the image orientation.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/rotation/or
     *
     * @return $this
     */
    public function orientation($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->orientation($v0);
    }

    /**
     * Changes the format of the output image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/format/fm
     *
     * @return $this
     */
    public function outputFormat($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->outputFormat($v0);
    }

    /**
     * Adjusts the quality of an output image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/format/q
     *
     * @return $this
     */
    public function outputQuality($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->outputQuality($v0);
    }

    /**
     * Pads an image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/border-and-padding/pad
     *
     * @return $this
     */
    public function padding($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->padding($v0);
    }

    /**
     * Specifies an output format for palette-extraction.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/color-palette/palette
     *
     * @return $this
     */
    public function paletteExtraction($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->paletteExtraction($v0);
    }

    /**
     * Selects a page from a PDF for display.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/pdf-page-number
     *
     * @return $this
     */
    public function pdfPageNumber($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->pdfPageNumber($v0);
    }

    /**
     * Applies a pixelation effect to an image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/stylize/px
     *
     * @return $this
     */
    public function pixellate($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->pixellate($v0);
    }

    /**
     * Rotates an image by a specified number of degrees.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/rotation/rot
     *
     * @return $this
     */
    public function rotationAngle($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->rotationAngle($v0);
    }

    /**
     * Adjusts the saturation of an image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/adjustment/sat
     *
     * @return $this
     */
    public function saturation($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->saturation($v0);
    }

    /**
     * Applies a sepia effect to an image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/stylize/sepia
     *
     * @return $this
     */
    public function sepia($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->sepia($v0);
    }

    /**
     * Adjusts the highlights of the source image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/adjustment/shad
     *
     * @return $this
     */
    public function shadow($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->shadow($v0);
    }

    /**
     * Adjusts the sharpness of the source image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/adjustment/sharp
     *
     * @return $this
     */
    public function sharpen($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->sharpen($v0);
    }

    /**
     * Sets the vertical and horizontal alignment of rendered text relative to the base image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/text/txtalign
     *
     * @return $this
     */
    public function textAlign($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->textAlign($v0);
    }

    /**
     * Sets the clipping properties of rendered text.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/text/txtclip
     *
     * @return $this
     */
    public function textClipping($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->textClipping($v0);
    }

    /**
     * Specifies the color of rendered text.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/text/txtclr
     *
     * @return $this
     */
    public function textColor($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->textColor($v0);
    }

    /**
     * Specifies the fit approach for rendered text.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/text/txtfit
     *
     * @return $this
     */
    public function textFitMode($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->textFitMode($v0);
    }

    /**
     * Selects a font for rendered text.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/text/txtfont
     *
     * @return $this
     */
    public function textFont($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->textFont($v0);
    }

    /**
     * Sets the font size of rendered text.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/text/txtsize
     *
     * @return $this
     */
    public function textFontSize($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->textFontSize($v0);
    }

    /**
     * Sets the leading (line spacing) for rendered text. Only works on the multi-line text endpoint.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/text/txtlead
     *
     * @return $this
     */
    public function textLeading($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->textLeading($v0);
    }

    /**
     * Controls the level of ligature substitution
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/text/txtlig
     *
     * @return $this
     */
    public function textLigatures($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->textLigatures($v0);
    }

    /**
     * Outlines the rendered text with a specified color.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/text/txtline
     *
     * @return $this
     */
    public function textOutline($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->textOutline($v0);
    }

    /**
     * Specifies a text outline color.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/text/txtlineclr
     *
     * @return $this
     */
    public function textOutlineColor($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->textOutlineColor($v0);
    }

    /**
     * Specifies the padding (in device-independent pixels) between a textbox and the edges of the base image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/text/txtpad
     *
     * @return $this
     */
    public function textPadding($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->textPadding($v0);
    }

    /**
     * Applies a shadow to rendered text.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/text/txtshad
     *
     * @return $this
     */
    public function textShadow($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->textShadow($v0);
    }

    /**
     * Sets the text string to render.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/text/txt
     *
     * @return $this
     */
    public function textString($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->textString($v0);
    }

    /**
     * Sets the tracking (letter spacing) for rendered text. Only works on the multi-line text endpoint.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/text/txtlead
     *
     * @return $this
     */
    public function textTracking($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->textTracking($v0);
    }

    /**
     * Sets the width of rendered text.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/text/txtwidth
     *
     * @return $this
     */
    public function textWidth($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->textWidth($v0);
    }

    /**
     * Specifies a trim color on a trim operation.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/trim/trimcolor
     *
     * @return $this
     */
    public function trimColor($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->trimColor($v0);
    }

    /**
     * Specifies the mean difference on a trim operation.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/trim/trimmd
     *
     * @return $this
     */
    public function trimMeanDifference($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->trimMeanDifference($v0);
    }

    /**
     * Trims the source image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/trim/trim
     *
     * @return $this
     */
    public function trimMode($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->trimMode($v0);
    }

    /**
     * Specifies the standard deviation on a trim operation.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/trim/trimsd
     *
     * @return $this
     */
    public function trimStandardDeviation($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->trimStandardDeviation($v0);
    }

    /**
     * Specifies the tolerance on a trim operation.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/trim/trimtol
     *
     * @return $this
     */
    public function trimTolerance($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->trimTolerance($v0);
    }

    /**
     * A Unix timestamp specifying a UTC time. Requests made to this URL after that time will output a 404 status code.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/expires
     *
     * @return $this
     */
    public function uRLExpirationTimestamp($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->uRLExpirationTimestamp($v0);
    }

    /**
     * Sharpens the source image using an unsharp mask.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/adjustment/usm
     *
     * @return $this
     */
    public function unsharpMask($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->unsharpMask($v0);
    }

    /**
     * Specifies the radius for an unsharp mask operation.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/adjustment/usmrad
     *
     * @return $this
     */
    public function unsharpMaskRadius($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->unsharpMaskRadius($v0);
    }

    /**
     * Adjusts the vibrance of an image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/adjustment/vib
     *
     * @return $this
     */
    public function vibrance($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->vibrance($v0);
    }

    /**
     * Changes the watermark alignment relative to the parent image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/watermark/markalign
     *
     * @return $this
     */
    public function watermarkAlignmentMode($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->watermarkAlignmentMode($v0);
    }

    /**
     * Changes the alpha of the watermark image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/watermark/markalpha
     *
     * @return $this
     */
    public function watermarkAlpha($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->watermarkAlpha($v0);
    }

    /**
     * Specifies the fit mode for watermark images.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/watermark/markfit
     *
     * @return $this
     */
    public function watermarkFitMode($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->watermarkFitMode($v0);
    }

    /**
     * Adjusts the height of the watermark image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/watermark/markh
     *
     * @return $this
     */
    public function watermarkHeight($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->watermarkHeight($v0);
    }

    /**
     * Specifies the location of the watermark image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/watermark/mark
     *
     * @return $this
     */
    public function watermarkImage($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->watermarkImage($v0);
    }

    /**
     * Applies padding to the watermark image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/watermark/markpad
     *
     * @return $this
     */
    public function watermarkPadding($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->watermarkPadding($v0);
    }

    /**
     * Adjusts the scale of the watermark image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/watermark/markscale
     *
     * @return $this
     */
    public function watermarkScale($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->watermarkScale($v0);
    }

    /**
     * Changes base URL of the watermark image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/watermark/markbase
     *
     * @return $this
     */
    public function watermarkUrlBase($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->watermarkUrlBase($v0);
    }

    /**
     * Adjusts the width of the watermark image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/watermark/markw
     *
     * @return $this
     */
    public function watermarkWidth($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->watermarkWidth($v0);
    }

    /**
     * Adjusts the x-offset of the watermark image relative to its parent.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/watermark/markx
     *
     * @return $this
     */
    public function watermarkXPosition($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->watermarkXPosition($v0);
    }

    /**
     * Adjusts the y-offset of the watermark image relative to its parent.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/watermark/marky
     *
     * @return $this
     */
    public function watermarkYPosition($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->watermarkYPosition($v0);
    }

    /**
     * Adjusts the width of the output image.
     * You can unset the value by passing nothing or null
     *
     * @see https://docs.imgix.com/apis/url/size/w
     *
     * @return $this
     */
    public function width($v0 = null): UrlBuilder
    {
        return app('imgix')->file($this->location)->width($v0);
    }
}
