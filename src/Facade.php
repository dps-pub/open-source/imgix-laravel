<?php namespace DPS\Imgix\Laravel;

class Facade extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return 'imgix';
    }
}
