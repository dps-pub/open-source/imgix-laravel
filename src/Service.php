<?php namespace DPS\Imgix\Laravel;

use Illuminate\Contracts\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use DPS\Imgix\UrlBuilder as Builder;
use Imgix\ShardStrategy;


class Service
{
    protected $domains;

    public function __construct($domains)
    {
        $this->domains = $domains;
    }

    public function builder()
    {
        return new Builder($this->domains);
    }

    public function file($filename)
    {
        return $this->builder()->file($filename);
    }
}