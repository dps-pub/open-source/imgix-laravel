<?php

namespace DPS\Imgix\Laravel;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        // publish the config
        $this->publishes([
            __DIR__.'/../config/imgix.php' => config_path('imgix.php'),
        ]);

        // Load migrations
        $this->loadMigrationsFrom(__DIR__.'/../migrations');
    }

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/imgix.php', 'imgix'
        );

        $this->app->singleton('imgix', function ($app) {
            return new Service(config('imgix.hosts'));
        });
    }
}
