<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImgixTables extends Migration
{
    public function up()
    {
        Schema::create('Media', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->string('filename');

            $table->smallInteger('faces')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('Media');
    }
}
