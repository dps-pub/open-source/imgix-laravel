<?php

namespace DPS\Imgix\Laravel\Tests;

use DPS\Imgix\Laravel\Models\Media;

class MediaTest extends TestCase
{
    protected $media;

    public function setUp(): void
    {
        parent::setUp();

        $this->media = Media::create([
            'filename' => 'test.png'
        ]);
    }

    function testAutoFeatures0()
    {
        $actual = (string) $this->media->autoFeatures('enhance');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?auto=enhance";
        $this->assertEquals($expected, $actual);
    }

    function testBlendAlign0()
    {
        $actual = (string) $this->media->blendAlign('top');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?ba=top";
        $this->assertEquals($expected, $actual);
    }

    function testBlendAlpha0()
    {
        $actual = (string) $this->media->blendAlpha(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?balph=1";
        $this->assertEquals($expected, $actual);
    }

    function testBlendCrop0()
    {
        $actual = (string) $this->media->blendCrop('top');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?bc=top";
        $this->assertEquals($expected, $actual);
    }

    function testBlendFit0()
    {
        $actual = (string) $this->media->blendFit('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?bf=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testBackgroundColor0()
    {
        $actual = (string) $this->media->backgroundColor('fff');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?bg=fff";
        $this->assertEquals($expected, $actual);
    }

    function testBlendHeight0()
    {
        $actual = (string) $this->media->blendHeight(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?bh=1";
        $this->assertEquals($expected, $actual);
    }

    function testBlendHeight1()
    {
        $actual = (string) $this->media->blendHeight(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?bh=1";
        $this->assertEquals($expected, $actual);
    }

    function testBlend0()
    {
        $actual = (string) $this->media->blend('fff');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?blend=fff";
        $this->assertEquals($expected, $actual);
    }

    function testBlend1()
    {
        $actual = (string) $this->media->blend('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?blend=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testBlend2()
    {
        $actual = (string) $this->media->blend('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?blend=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testBlur0()
    {
        $actual = (string) $this->media->blur(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?blur=1";
        $this->assertEquals($expected, $actual);
    }

    function testBlendMode0()
    {
        $actual = (string) $this->media->blendMode('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?bm=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testBorderRadiusInner0()
    {
        $actual = (string) $this->media->borderRadiusInner(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?border-radius-inner=1";
        $this->assertEquals($expected, $actual);
    }

    function testBorderRadiusInner1()
    {
        $actual = (string) $this->media->borderRadiusInner(1,1,1,1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?border-radius-inner=1%2C1%2C1%2C1";
        $this->assertEquals($expected, $actual);
    }

    function testBorderRadius0()
    {
        $actual = (string) $this->media->borderRadius(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?border-radius=1";
        $this->assertEquals($expected, $actual);
    }

    function testBorderRadius1()
    {
        $actual = (string) $this->media->borderRadius(1,1,1,1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?border-radius=1%2C1%2C1%2C1";
        $this->assertEquals($expected, $actual);
    }

    function testBorder0()
    {
        $actual = (string) $this->media->border(1,'fff');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?border=1%2Cfff";
        $this->assertEquals($expected, $actual);
    }

    function testBlendPadding0()
    {
        $actual = (string) $this->media->blendPadding(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?bp=1";
        $this->assertEquals($expected, $actual);
    }

    function testBrightness0()
    {
        $actual = (string) $this->media->brightness(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?bri=1";
        $this->assertEquals($expected, $actual);
    }

    function testBlendSize0()
    {
        $actual = (string) $this->media->blendSize('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?bs=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testBlendWidth0()
    {
        $actual = (string) $this->media->blendWidth(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?bw=1";
        $this->assertEquals($expected, $actual);
    }

    function testBlendWidth1()
    {
        $actual = (string) $this->media->blendWidth(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?bw=1";
        $this->assertEquals($expected, $actual);
    }

    function testBlendX0()
    {
        $actual = (string) $this->media->blendX(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?bx=1";
        $this->assertEquals($expected, $actual);
    }

    function testBlendY0()
    {
        $actual = (string) $this->media->blendY(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?by=1";
        $this->assertEquals($expected, $actual);
    }

    function testClientHints0()
    {
        $actual = (string) $this->media->clientHints('width');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?ch=width";
        $this->assertEquals($expected, $actual);
    }

    function testChromaSubsampling0()
    {
        $actual = (string) $this->media->chromaSubsampling(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?chromasub=1";
        $this->assertEquals($expected, $actual);
    }

    function testColorQuantization0()
    {
        $actual = (string) $this->media->colorQuantization(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?colorquant=1";
        $this->assertEquals($expected, $actual);
    }

    function testColorCount0()
    {
        $actual = (string) $this->media->colorCount(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?colors=1";
        $this->assertEquals($expected, $actual);
    }

    function testContrast0()
    {
        $actual = (string) $this->media->contrast(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?con=1";
        $this->assertEquals($expected, $actual);
    }

    function testCornerRadius0()
    {
        $actual = (string) $this->media->cornerRadius(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?corner-radius=1";
        $this->assertEquals($expected, $actual);
    }

    function testCornerRadius1()
    {
        $actual = (string) $this->media->cornerRadius(1,1,1,1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?corner-radius=1%2C1%2C1%2C1";
        $this->assertEquals($expected, $actual);
    }

    function testCropMode0()
    {
        $actual = (string) $this->media->cropMode('top');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?crop=top";
        $this->assertEquals($expected, $actual);
    }

    function testColorspace0()
    {
        $actual = (string) $this->media->colorspace('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?cs=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testDownload0()
    {
        $actual = (string) $this->media->download('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?dl=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testDotsPerInch0()
    {
        $actual = (string) $this->media->dotsPerInch(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?dpi=1";
        $this->assertEquals($expected, $actual);
    }

    function testDevicePixelRatio0()
    {
        $actual = (string) $this->media->devicePixelRatio(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?dpr=1";
        $this->assertEquals($expected, $actual);
    }

    function testExposure0()
    {
        $actual = (string) $this->media->exposure(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?exp=1";
        $this->assertEquals($expected, $actual);
    }

    function testURLExpirationTimestamp0()
    {
        $actual = (string) $this->media->uRLExpirationTimestamp('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?expires=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testFaceIndex0()
    {
        $actual = (string) $this->media->faceIndex(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?faceindex=1";
        $this->assertEquals($expected, $actual);
    }

    function testFacePadding0()
    {
        $actual = (string) $this->media->facePadding(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?facepad=1";
        $this->assertEquals($expected, $actual);
    }

    function testFaces0()
    {
        $actual = (string) $this->media->faces(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?faces=1";
        $this->assertEquals($expected, $actual);
    }

    function testFitMode0()
    {
        $actual = (string) $this->media->fitMode('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?fit=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testFlipDirection0()
    {
        $actual = (string) $this->media->flipDirection('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?flip=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testOutputFormat0()
    {
        $actual = (string) $this->media->outputFormat('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?fm=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testFocalPointCrosshairs0()
    {
        $actual = (string) $this->media->focalPointCrosshairs(true);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?fp-debug=1";
        $this->assertEquals($expected, $actual);
    }

    function testFocalPointXValue0()
    {
        $actual = (string) $this->media->focalPointXValue(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?fp-x=1";
        $this->assertEquals($expected, $actual);
    }

    function testFocalPointYValue0()
    {
        $actual = (string) $this->media->focalPointYValue(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?fp-y=1";
        $this->assertEquals($expected, $actual);
    }

    function testFocalPointZoomValue0()
    {
        $actual = (string) $this->media->focalPointZoomValue(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?fp-z=1";
        $this->assertEquals($expected, $actual);
    }

    function testGamma0()
    {
        $actual = (string) $this->media->gamma(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?gam=1";
        $this->assertEquals($expected, $actual);
    }

    function testHeight0()
    {
        $actual = (string) $this->media->height(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?h=1";
        $this->assertEquals($expected, $actual);
    }

    function testHeight1()
    {
        $actual = (string) $this->media->height(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?h=1";
        $this->assertEquals($expected, $actual);
    }

    function testHigh0()
    {
        $actual = (string) $this->media->high(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?high=1";
        $this->assertEquals($expected, $actual);
    }

    function testHalftone0()
    {
        $actual = (string) $this->media->halftone(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?htn=1";
        $this->assertEquals($expected, $actual);
    }

    function testHueShift0()
    {
        $actual = (string) $this->media->hueShift(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?hue=1";
        $this->assertEquals($expected, $actual);
    }

    function testInvert0()
    {
        $actual = (string) $this->media->invert(true);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?invert=1";
        $this->assertEquals($expected, $actual);
    }

    function testLossless0()
    {
        $actual = (string) $this->media->lossless(true);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?lossless=1";
        $this->assertEquals($expected, $actual);
    }

    function testWatermarkImage0()
    {
        $actual = (string) $this->media->watermarkImage('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?mark=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testWatermarkImage1()
    {
        $actual = (string) $this->media->watermarkImage('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?mark=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testWatermarkAlignmentMode0()
    {
        $actual = (string) $this->media->watermarkAlignmentMode('top');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?markalign=top";
        $this->assertEquals($expected, $actual);
    }

    function testWatermarkAlpha0()
    {
        $actual = (string) $this->media->watermarkAlpha(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?markalpha=1";
        $this->assertEquals($expected, $actual);
    }

    function testWatermarkUrlBase0()
    {
        $actual = (string) $this->media->watermarkUrlBase('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?markbase=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testWatermarkUrlBase1()
    {
        $actual = (string) $this->media->watermarkUrlBase('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?markbase=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testWatermarkFitMode0()
    {
        $actual = (string) $this->media->watermarkFitMode('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?markfit=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testWatermarkHeight0()
    {
        $actual = (string) $this->media->watermarkHeight(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?markh=1";
        $this->assertEquals($expected, $actual);
    }

    function testWatermarkHeight1()
    {
        $actual = (string) $this->media->watermarkHeight(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?markh=1";
        $this->assertEquals($expected, $actual);
    }

    function testWatermarkPadding0()
    {
        $actual = (string) $this->media->watermarkPadding(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?markpad=1";
        $this->assertEquals($expected, $actual);
    }

    function testWatermarkScale0()
    {
        $actual = (string) $this->media->watermarkScale(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?markscale=1";
        $this->assertEquals($expected, $actual);
    }

    function testWatermarkWidth0()
    {
        $actual = (string) $this->media->watermarkWidth(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?markw=1";
        $this->assertEquals($expected, $actual);
    }

    function testWatermarkWidth1()
    {
        $actual = (string) $this->media->watermarkWidth(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?markw=1";
        $this->assertEquals($expected, $actual);
    }

    function testWatermarkXPosition0()
    {
        $actual = (string) $this->media->watermarkXPosition(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?markx=1";
        $this->assertEquals($expected, $actual);
    }

    function testWatermarkYPosition0()
    {
        $actual = (string) $this->media->watermarkYPosition(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?marky=1";
        $this->assertEquals($expected, $actual);
    }

    function testMask0()
    {
        $actual = (string) $this->media->mask('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?mask=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testMask1()
    {
        $actual = (string) $this->media->mask('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?mask=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testMask2()
    {
        $actual = (string) $this->media->mask('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?mask=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testMaskBackgroundColor0()
    {
        $actual = (string) $this->media->maskBackgroundColor('fff');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?maskbg=fff";
        $this->assertEquals($expected, $actual);
    }

    function testMaximumHeight0()
    {
        $actual = (string) $this->media->maximumHeight(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?max-h=1";
        $this->assertEquals($expected, $actual);
    }

    function testMaximumWidth0()
    {
        $actual = (string) $this->media->maximumWidth(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?max-w=1";
        $this->assertEquals($expected, $actual);
    }

    function testMinimumHeight0()
    {
        $actual = (string) $this->media->minimumHeight(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?min-h=1";
        $this->assertEquals($expected, $actual);
    }

    function testMinimumWidth0()
    {
        $actual = (string) $this->media->minimumWidth(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?min-w=1";
        $this->assertEquals($expected, $actual);
    }

    function testMonochrome0()
    {
        $actual = (string) $this->media->monochrome('fff');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?mono=fff";
        $this->assertEquals($expected, $actual);
    }

    function testNoiseBlur0()
    {
        $actual = (string) $this->media->noiseBlur(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?nr=1";
        $this->assertEquals($expected, $actual);
    }

    function testNoiseSharpen0()
    {
        $actual = (string) $this->media->noiseSharpen(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?nrs=1";
        $this->assertEquals($expected, $actual);
    }

    function testOrientation0()
    {
        $actual = (string) $this->media->orientation(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?or=1";
        $this->assertEquals($expected, $actual);
    }

    function testPadding0()
    {
        $actual = (string) $this->media->padding(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?pad=1";
        $this->assertEquals($expected, $actual);
    }

    function testPdfPageNumber0()
    {
        $actual = (string) $this->media->pdfPageNumber(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?page=1";
        $this->assertEquals($expected, $actual);
    }

    function testPaletteExtraction0()
    {
        $actual = (string) $this->media->paletteExtraction('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?palette=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testCssPrefix0()
    {
        $actual = (string) $this->media->cssPrefix('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?prefix=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testPixellate0()
    {
        $actual = (string) $this->media->pixellate(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?px=1";
        $this->assertEquals($expected, $actual);
    }

    function testOutputQuality0()
    {
        $actual = (string) $this->media->outputQuality(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?q=1";
        $this->assertEquals($expected, $actual);
    }

    function testCropRectangle0()
    {
        $actual = (string) $this->media->cropRectangle('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?rect=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testRotationAngle0()
    {
        $actual = (string) $this->media->rotationAngle(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?rot=1";
        $this->assertEquals($expected, $actual);
    }

    function testSaturation0()
    {
        $actual = (string) $this->media->saturation(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?sat=1";
        $this->assertEquals($expected, $actual);
    }

    function testSepia0()
    {
        $actual = (string) $this->media->sepia(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?sepia=1";
        $this->assertEquals($expected, $actual);
    }

    function testShadow0()
    {
        $actual = (string) $this->media->shadow(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?shad=1";
        $this->assertEquals($expected, $actual);
    }

    function testSharpen0()
    {
        $actual = (string) $this->media->sharpen(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?sharp=1";
        $this->assertEquals($expected, $actual);
    }

    function testTrimMode0()
    {
        $actual = (string) $this->media->trimMode('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?trim=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testTrimColor0()
    {
        $actual = (string) $this->media->trimColor('fff');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?trimcolor=fff";
        $this->assertEquals($expected, $actual);
    }

    function testTrimMeanDifference0()
    {
        $actual = (string) $this->media->trimMeanDifference(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?trimmd=1";
        $this->assertEquals($expected, $actual);
    }

    function testTrimStandardDeviation0()
    {
        $actual = (string) $this->media->trimStandardDeviation(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?trimsd=1";
        $this->assertEquals($expected, $actual);
    }

    function testTrimTolerance0()
    {
        $actual = (string) $this->media->trimTolerance(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?trimtol=1";
        $this->assertEquals($expected, $actual);
    }

    function testTextString0()
    {
        $actual = (string) $this->media->textString('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?txt=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testTextAlign0()
    {
        $actual = (string) $this->media->textAlign('top');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?txtalign=top";
        $this->assertEquals($expected, $actual);
    }

    function testTextClipping0()
    {
        $actual = (string) $this->media->textClipping('start');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?txtclip=start";
        $this->assertEquals($expected, $actual);
    }

    function testTextColor0()
    {
        $actual = (string) $this->media->textColor('fff');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?txtclr=fff";
        $this->assertEquals($expected, $actual);
    }

    function testTextFitMode0()
    {
        $actual = (string) $this->media->textFitMode('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?txtfit=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testTextFont0()
    {
        $actual = (string) $this->media->textFont('unknown');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?txtfont=unknown";
        $this->assertEquals($expected, $actual);
    }

    function testTextLeading0()
    {
        $actual = (string) $this->media->textLeading(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?txtlead=1";
        $this->assertEquals($expected, $actual);
    }

    function testTextLigatures0()
    {
        $actual = (string) $this->media->textLigatures(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?txtlig=1";
        $this->assertEquals($expected, $actual);
    }

    function testTextOutline0()
    {
        $actual = (string) $this->media->textOutline(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?txtline=1";
        $this->assertEquals($expected, $actual);
    }

    function testTextOutlineColor0()
    {
        $actual = (string) $this->media->textOutlineColor('fff');
        $expected = "https://foo.imgix.net/{$this->media->id}.png?txtlineclr=fff";
        $this->assertEquals($expected, $actual);
    }

    function testTextPadding0()
    {
        $actual = (string) $this->media->textPadding(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?txtpad=1";
        $this->assertEquals($expected, $actual);
    }

    function testTextShadow0()
    {
        $actual = (string) $this->media->textShadow(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?txtshad=1";
        $this->assertEquals($expected, $actual);
    }

    function testTextFontSize0()
    {
        $actual = (string) $this->media->textFontSize(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?txtsize=1";
        $this->assertEquals($expected, $actual);
    }

    function testTextTracking0()
    {
        $actual = (string) $this->media->textTracking(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?txttrack=1";
        $this->assertEquals($expected, $actual);
    }

    function testTextWidth0()
    {
        $actual = (string) $this->media->textWidth(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?txtwidth=1";
        $this->assertEquals($expected, $actual);
    }

    function testUnsharpMask0()
    {
        $actual = (string) $this->media->unsharpMask(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?usm=1";
        $this->assertEquals($expected, $actual);
    }

    function testUnsharpMaskRadius0()
    {
        $actual = (string) $this->media->unsharpMaskRadius(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?usmrad=1";
        $this->assertEquals($expected, $actual);
    }

    function testVibrance0()
    {
        $actual = (string) $this->media->vibrance(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?vib=1";
        $this->assertEquals($expected, $actual);
    }

    function testWidth0()
    {
        $actual = (string) $this->media->width(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?w=1";
        $this->assertEquals($expected, $actual);
    }

    function testWidth1()
    {
        $actual = (string) $this->media->width(1);
        $expected = "https://foo.imgix.net/{$this->media->id}.png?w=1";
        $this->assertEquals($expected, $actual);
    }
}
